use std::io;
use std::mem;
//use std::str;

const ALLOWED_BLOCK_SIZES: [u32; 4] = [0x0001_0000, 0x0004_0000, 0x0010_0000, 0x0040_0000];

macro_rules! decode_from_reader {
	($t:ty, $reader:ident, $buffer:ident) => {{
		$reader
			.read_exact(&mut $buffer[0..mem::size_of::<$t>()])
			.unwrap();
		<$t>::from_le(
			$buffer
				.iter()
				.rev()
				.fold(0 as $t, |acc, x| (acc << 8) | <$t>::from(*x)),
		)
		}};
}

#[derive(Debug)]
enum FrameMagicNumError {
	Skippable(u32),
	Legacy(u32),
	Unexpected(u32),
}

fn check_magic_num(num: u32) -> Result<(), FrameMagicNumError> {
	match num {
		0x184d_2204 => Ok(()),
		//TODO allow custom callback for user defined frame
		0x184d_2a50...0x184d_2a5f => Err(FrameMagicNumError::Skippable(num)),
		0x184c_2102 => Err(FrameMagicNumError::Legacy(num)),
		_ => Err(FrameMagicNumError::Unexpected(num)),
	}
}

fn check_version_num(num: u8) -> Result<(), String> {
	match num >> 6 {
		1 => Ok(()),
		x => Err(format!("Unexpected Frame Version encountered : {:02}", x)),
	}
}

fn check_reserved_bits(num: u8, bit_pattern: u8) -> Result<(), String> {
	match num & bit_pattern {
		0 => Ok(()),
		_ => Err(format!(
			"Unexpected Reserved bits are set: num {:08b}, pattern {:08b}",
			num, bit_pattern
		)),
	}
}

pub fn decode_frame<R>(mut reader: R) -> Result<(), String>
where
	R: io::Read,
{
	let mut tmpbuff = [0u8; 0xffff];
	let magic_num = decode_from_reader!(u32, reader, tmpbuff);
	check_magic_num(magic_num).unwrap();
	reader.read_exact(&mut tmpbuff[0..2]).unwrap();

	check_version_num(tmpbuff[0])?;
	check_reserved_bits(tmpbuff[0], 0b0000_0010)?;
	let dict_id: bool = (tmpbuff[0] & 0b0000_0001) != 0;
	let c_checksum: bool = (tmpbuff[0] & 0b0000_0100) != 0;
	let c_size: bool = (tmpbuff[0] & 0b0000_1000) != 0;
	let b_checksum: bool = (tmpbuff[0] & 0b0001_0000) != 0;
	//let b_indep: bool = (tmpbuff[0] & 0b0010_0000) != 0;

	check_reserved_bits(tmpbuff[1], 0b1000_1111)?;
	let block_max_size = ALLOWED_BLOCK_SIZES[((tmpbuff[1] >> 4) - 4) as usize];

	if dict_id {
		return Err("Decoder does not support dictionaries".to_string());
	}

	if c_size {
		let uncompressed_frame_size = decode_from_reader!(u64, reader, tmpbuff);
		println!("uncompressed size = {} bytes", uncompressed_frame_size);
	};

	//this is always false at this point
	if dict_id {
		reader.read_exact(&mut tmpbuff[0..4]).unwrap();
	}
	//skip header checksum
	reader.read_exact(&mut tmpbuff[0..1]).unwrap();

	loop {
		let block_size = decode_from_reader!(u32, reader, tmpbuff);
		if block_size == 0 {
			break;
		}
		let mut block_size_real = block_size & 0x7fff_ffff;
		if block_size_real > block_max_size {
			return Err(format!(
				"Unexpected Block Size encountered : {}, max allowed : {}",
				block_size_real, block_max_size
			));
		}
		if (block_size >> 31) == 0 {
			while block_size_real > 0 {
				decode_block(&mut reader, &mut block_size_real);
			}
		} else {
			//use temp buffer until i come up with a better solution
			let mut uncompressed_tmp_buff = vec![0_u8; block_size_real as usize];
			reader.read_exact(&mut uncompressed_tmp_buff).unwrap();
		}
		if b_checksum {
			reader.read_exact(&mut tmpbuff[0..4]).unwrap();
		}
	}

	if c_checksum {
		reader.read_exact(&mut tmpbuff[0..4]).unwrap();
	}

	Ok(())
}

fn decode_block<R>(a: &mut R, block_size: &mut u32)
where
	R: io::Read,
{
	let mut token = [0_u8; 1];
	a.read_exact(&mut token).unwrap();
	let num_literals = decode_var_num(a, 0, token[0] >> 4);
	// assert(blockSize >= (1 + num_literals.first));  // underflow
	*block_size -= 1 + num_literals.0;
	let mut uncompressed_tmp_buff = vec![0_u8; num_literals.0 as usize];
	a.read_exact(&mut uncompressed_tmp_buff).unwrap();
	// println!("{:?}", str::from_utf8(&uncompressed_tmp_buff).unwrap());
	// assert(blockSize >= num_literals.second);  // underflow
	*block_size -= num_literals.1;
	if *block_size == 0 {
		return;
	}
	let mut tmp = [0_u8; 4];
	let offset = decode_from_reader!(u16, a, tmp);
	// assert(offset != 0);                       // Invalid Offset value
	// assert(m_decodedBuffer.size() >= offset);  // underflow
	// auto offset_idx = m_decodedBuffer.size() - offset;
	let match_length = decode_var_num(a, 4, token[0] & 0x0f);
	// while (match_length.first--)
	// {
	// 	m_decodedBuffer.emplace_back(m_decodedBuffer[offset_idx]);
	// 	++offset_idx;
	// }
	// assert(blockSize >= (2 + match_length.second));  // underflow
	*block_size -= 2 + match_length.1;
}

fn decode_var_num<R>(a: &mut R, offset: u32, token: u8) -> (u32, u32)
where
	R: io::Read,
{
	let mut sum_ret = u32::from(token) + offset;
	if token < 15 {
		return (sum_ret, 0);
	}
	let mut next = [0_u8; 1];
	let mut extra_bytes = 0_u32;
	loop {
		a.read_exact(&mut next).unwrap();
		extra_bytes += 1;
		// assert((std::numeric_limits<size_t>::max() - sum_ret) >= next);  // overflow
		sum_ret += u32::from(next[0]);
		if next[0] != 0xff {
			break;
		}
	}
	(sum_ret, extra_bytes)
}
