use std::fs::File;

mod lz4;

fn main() -> std::io::Result<()> {
	let mut file = File::open("bible.txt.lz4")?;
	//let mut output = vec![];
	lz4::decode_frame(&mut file).unwrap();
	//println!("{:?}", output);
	Ok(())
}
